<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>

    <?php 
        echo "<h3> Soal No 1 Greetings </h3>";

        function greetings($nama) {
            echo "Halo {$nama}, Selamat Datang di Sanbercode <br>";
        }
        
        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");

        echo "<h3>Soal No 2 Reverse String</h3>";

        function reverseString($nama) {
            $tampung = "";
            for($i = strlen($nama) - 1; $i >= 0; $i--) {
                $tampung .= $nama[$i];
                
            }
            echo "{$tampung} <br>";
            return $tampung;
        }

        reverseString("abdul");
        reverseString("Sanbercode");
        reverseString("We Are Sanbers Developers");

        echo "<h3>Soal No 3 Palindrome </h3>";

        function palindrome($nama) {
            $tampung = "";
            for($i = strlen($nama) - 1; $i >= 0; $i--) {
                $tampung .= $nama[$i];
                
            }
            
            if($tampung == $nama) {
                echo "true <br>";
            } else {
                echo "false <br>";
            }
        }

        palindrome("civic") ; 
        palindrome("nababan") ; 
        palindrome("jambaban"); 
        palindrome("racecar");

        echo "<h3>Soal No 4 Tentukan Nilai </h3>";
        
        function tentukan_nilai($nilai) {
            if($nilai >= 85) {
                echo "Sangat Baik <br>";
            } elseif ($nilai >= 70) {
                echo "Baik <br>";
            } elseif ($nilai >= 60) {
                echo "Cukup <br>";
            } else {
                echo "Kurang <br>";
            }

        }

        tentukan_nilai(98); 
        tentukan_nilai(76); 
        tentukan_nilai(67); 
        tentukan_nilai(43);
    ?>
</body>
</html>