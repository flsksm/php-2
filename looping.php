<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";

        echo "LOOPING PERTAMA <br>";
        for ($i = 2; $i <= 20; $i += 2) { 
            echo "{$i} - I Love PHP <br>";
        }

        echo "LOOPING KEDUA <br>";
        for ($i = 20; $i >= 2; $i -= 2) { 
            echo "{$i} - I Love PHP <br>";
        }

        echo "<h3>Soal No 2 Looping Array Modulo</h3>";

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);

        echo "<br>";
        //looping untuk akses value dari array
        foreach($numbers as $values) {
            $rest[] = $values % 5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah: "; 
        print_r($rest);
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach($items as $values) {
            $isi = [
                'nama' => $values[1],
                'price' => $values[2],
                'description' => $values[3],
                'source' => $values[4]
            ];

            print_r($isi);
            echo "<br>";
        }

        echo "<h3>Soal No 4 Asterix </h3>";
        echo "Asterix: ";
        echo "<br>"; 

        for($i = 1; $i <= 5; $i++) {
            for($j = 1; $j <= $i; $j++) {
                echo "*";
            }
            echo "<br>"; 
        }
        
    ?>
</body>
</html>